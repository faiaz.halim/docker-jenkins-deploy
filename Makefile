include .env
SHELL = /bin/zsh
compose=docker-compose -f fig.$(ENV).yml

update:
	git pull
	cp .env_ .env

set_jenkins_permission:
	sudo chown 1000:1000 ./jenkins -R

#docker compose commands

compose-up:
	$(compose) up -d

compose-down:
	$(compose) down

compose-start:
	$(compose) start

compose-stop:
	$(compose) stop

compose-pull:
	$(compose) pull jenkins
	
compose-reload: compose-stop compose-start

compose-restart: compose-down compose-up

compose-status:
	$(compose) ps

compose-exec-jenkins:
	docker exec -ti $(JENKINS_CON) bash

get-initial-pass:
	docker exec -ti $(JENKINS_CON) cat /var/jenkins_home/secrets/initialAdminPassword